#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exemplo do Algoritmo k-NN para scikit-learn.
@author: michelgomes
"""
import sys
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

def main(fold1, fold2, fold3):

        # load data
        print ("Loading data...")

        #Folds de Treino
        fold1 = np.loadtxt(fold1)
        fold2 = np.loadtxt(fold2)

        #Concate os dois Folds de Treino
        tr = np.concatenate((fold1, fold2))

        #Fold de Teste
        ts = np.loadtxt(fold3)

        #Separa os labels e as caracteristicas do Treino
        X_train = tr[:, 1 : -1]
        y_train = tr[:,-1]

        #Separa Caracteristica e Label do Teste
        X_test = ts[:, 1 : -1]
        y_test = ts[:,-1]
        
        #Separa 20% para validação e 80% para o Treino
        X2_train, X_validation, y2_train, y_validation = train_test_split(X_train, y_train, test_size=0.20, random_state=42)
        
        print("Total do Treino: "+str(X_train.shape[0]))
        print("80% Treino: "+str(X2_train.shape[0]))
        print("20% Validação: "+str(X_validation.shape[0]))
        print("Caracteristicas: "+str(X_train.shape[1]))

        '''
        Escolher o melhor K
        '''
         # k-NN classifier
        i = 1
        maior_k = 0
        score_atual = 0
        while i < X2_train.shape[0]:
                neigh = KNeighborsClassifier(n_neighbors=i, metric='euclidean')
                neigh.fit(X2_train, y2_train)
                tmp = neigh.score(X_validation, y_validation)
                if tmp > score_atual:
                        score_atual = tmp
                        maior_k = i
                i += 2
        print("\n Melhor K["+str(maior_k)+"] ~ "+str(score_atual))

 # k-NN classifier
        neigh = KNeighborsClassifier(n_neighbors=maior_k, metric='euclidean')
        neigh.fit(X_train, y_train)
        print("Score: "+str(neigh.score(X_test, y_test)))

       
if __name__ == "__main__":

        fold1 = "LMD_Fold1_Inception_10x1.txt"
        fold2 = "LMD_Fold2_Inception_10x1.txt"
        fold3 = "LMD_Fold3_Inception_10x1.txt"
        print("\n---------------------10x1----------------------")
        print("\n\n[1],[3] -> [2]")
        main(fold1, fold3, fold2)
        #print("\n\n[2],[3] -> [1]")
        #main(fold2, fold3, fold1)

